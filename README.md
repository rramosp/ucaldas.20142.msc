# **Computación Paralela** 2014 - Semestre II

## Maestría en Ingeniería Computacional - Universidad de Caldas

Raúl Ramos-Pollán, Universidad Industrial de Santander, \[[+info](https://sites.google.com/site/rulixrp/courses)\] [rramosp@uis.edu.co](mailto:rramosp@uis.edu.co)

---

**Estaremos usando:**

+ una máquina virtual configurada para el curso, con CentOS 6, OpenMP, MPI y Python Anaconda para que puedas realizar de manera independiente tus ejercicios (_contáctame para obtenerla_)
+ la infraestructura `guane` de [Centro de Supercómputo y Cálculo Científico de la UIS](http://www.sc3.uis.edu.co) sobre todo para las pruebas de escalabilidad y uso de CUDA/GPUs (_instrucciones de acceso más abajo_)

**Crea una copia local de este repositorio para trabajar en los ejercicios desde la máquina virtual del curso y lanza tu servidor personal del entorno de ejecución de `ipython notebooks`**

    :::console
    git clone https://bitbucket.org/rramosp/ucaldas.20142.msc
    ipython notebook


## Lección 1 - Introducción

**Temas:** Uso infraestructura SC3-UIS, Paralelismo y computación, Computación distribuida, Arquitecturas de procesadores

**Refs:** Supercomputación y Cálculo Científico UIS [[web site](http://www.sc3.uis.edu.co/)], Caché effects [[enlace](http://igoro.com/archive/gallery-of-processor-cache-effects/)] , HPC Course @UWisc [[enlace](http://sbel.wisc.edu/Courses/ME964/2012/)]

**Ejercicios:**

+ [Lección 1 - C warmup](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-01-intro/0.C-warmup/C%20warmup.ipynb)
+ [Lección 1 - Alineamiento de Datos - parte 1](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-01-intro/1.data-alignment/alineamiento%20de%20datos%20-%20parte%201.ipynb)
+ [Lección 1 - Alineamiento de Datos - parte 2](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-01-intro/1.data-alignment/alineamiento%20de%20datos%20-%20parte%202.ipynb)
+ [Lección 1 - Líneas de Caché](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-01-intro/2.cache-lines/lineas%20de%20cache.ipynb)
+ [Lección 1 - Tamaños de Caché](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-01-intro/3.cache-sizes/tamanos%20de%20cache.ipynb)

## Lección 2 - OpenMP
**Temas:** Modelo de programación, Variables compartidas y sincronización, Tareas, Optimización

**Refs:** Tim Mattson, Intel [[video lectures](http://www.youtube.com/watch?v=nE-xN4Bf8XI&list=PLLX-Q6B8xqZ8n8bwjGdzBJ25X2utwnoEG), [materials](https://software.intel.com/en-us/courseware/249662)], Lista de recursos [[enlace](http://www.compunity.org/training/tutorials/)]

**Ejercicios:**

+ [Lección 2 - OpenMP warmup](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-02-openmp/1.omp-warmup/OMP%20warmup.ipynb)
+ [Lección 2 - PI paralelo](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-02-openmp/2.pi-paralelo/PI%20paralelo.ipynb)
+ [Lección 2 - PI mejorado](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-02-openmp/3.pi-mejorado/PI%20mejorado.ipynb)
+ [Lección 2 - Loops](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-02-openmp/4.loops/Loops.ipynb)
+ [Lección 2 - Reducers](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-02-openmp/4.loops/Reduce.ipynb)
+ [Lección 2 - Montecarlo](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-02-openmp/5.montecarlo/Montecarlo%20PI.ipynb)

## Lección 3 MPI
**Temas:** Modelo de programación, MPI Python y Numpy, Comunicaciones, Coordinación, Sincronización, Procesamiento de imágenes.

**Refs:** LLNL [[tutorial](https://computing.llnl.gov/tutorials/mpi/)]
ANL [[tutorial + info](http://www.mcs.anl.gov/research/projects/mpi/)]
Python MPI Groupcomms [[web page](http://mpitutorial.com/mpi-scatter-gather-and-allgather/)]


+ [Lección 3 - MPI Warmup](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-03-mpi/1.MPI-warmup/MPI%20warmup.ipynb)
+ [Lección 3 - MPI Python](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-03-mpi/2.MPI-Python/MPI%20Python.ipynb)
+ [Lección 3 - MPI Numpy](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-03-mpi/3.MPI-Numpy/MPI%20Numpy.ipynb)
+ [Lección 3 - MPI Comunicaciones grupo](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-03-mpi/4.MPI-Groupcomms/MPI%20Groupcomms.ipynb)
+ [Lección 3 - MPI Multiplicación Vectorial](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-03-mpi/5.MPI-Vectmult/MPI%20Vectmult.ipynb)
+ [Lección 3 - MPI Procesamiento de Imágenes](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-03-mpi/6.MPI-Image/MPI%20Image%20Processing.ipynb)

## Lección 4 CUDA
**Temas:** Arquitectura hardware, Modelo de programación, Coalescencia, Divergencia, Optimizaciones y _Best practices_

**Refs:**  NVIDIA CUDA Best Practices [[book](http://docs.nvidia.com/cuda/cuda-c-best-practices-guide/)], NVIDIA CUDA Programming Guide [[book](http://docs.nvidia.com/cuda/cuda-c-programming-guide/#axzz3CvyLDhr4)], Ejemplos de Python Numbapro [[repositorio Github](https://github.com/ContinuumIO/numbapro-examples)]

+ [Lección 4 - CUDA intro](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-04-cuda/0.%20CUDA%20Intro.ipynb)
+ [Lección 4 - CUDA Vectoradd](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-04-cuda/1.%20CUDA%20Vectoradd.ipynb)
+ [Lección 4 - CUDA Vector reduce modulus](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-04-cuda/2.%20CUDA%20Vector%20reduce%20modulus.ipynb)
+ [Lección 4 - CUDA memoria compartida](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-04-cuda/3.%20CUDA%20memoria%20compartida.ipynb)
+ [Lección 4 - CUDA pre-carga de datos](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-04-cuda/4.%20CUDA%20pre-carga%20de%20datos.ipynb)
+ [Lección 4 - CUDA  bucle desarrollado](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-04-cuda/5.%20CUDA%20bucle%20desarrollado.ipynb)

_demos con Python_

+ [Lección 4 - CUDA Apéndice Python Vectoradd](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-04-cuda/9.%20CUDA%20Ap%C3%A9ndice%20Python%20Vectoradd.ipynb)
+ [Lección 4 - CUDA Apéndice Python Mandelbrot](http://nbviewer.ipython.org/urls/bitbucket.org/rramosp/ucaldas.20142.msc/raw/master/leccion-04-cuda/9.%20CUDA%20Ap%C3%A9ndice%20Python%20Mandelbrot.ipynb)

## Uso infraestructura **GUANE** @ **UIS**

si, por ejemplo, tu número de estudiante es el 03 usa la cuenta `estudiante03` y el puerto `9903` en los comandos siguientes.

Entra en la máquina de salida de la Universidad de Caldas:

    :::console
    ssh progpara@c-head.ucaldas.edu.co -L9903:localhost:9903

Usa la cuenta compartida para entrar en la máquina `toctoc` y en la máquina `guane` que son los puntos de entrada para la infraestructura de la UIS:

    :::console
    ssh ucaldas@toctoc.grid.uis.edu.co -o ServerAliveInterval=30 -L9903:localhost:9903

    ssh ucaldas@guane -L9903:localhost:9903


Una vez en `guane`, usa tu cuenta para entrar en la máquina reservada para el curso (p.ej. `guane01`, confirma con el profesor cuál está disponible durante el curso):

    :::console
    ssh estudiante03@guane01 -L9903:localhost:9903


Si es la primera vez que entras, crea una copia del repositorio de ejercicios para poder trabajarlos de manera independiente 

    :::console
    git clone https://bitbucket.org/rramosp/ucaldas.20142.msc


Lanza tu servidor personal de notebooks de ipython:

    :::console
    ipython notebook --port 9903 --no-browser


Abre la siguiente dirección en un browser de tu computadora de escritorio

    :::console
    http://localhost:9903