
#include <stdlib.h>
#include "rlxutils.h"



int main(int argc, char **argv) {
  struct timespec tsi, tsf;

  long KB = 1024;
  long N = 20;
  long P2 = 16;

  long i,j,l,k;

  double time_difs[N];

    char *fname = "cache_size.data";
    printf ("writing to file %s\n", fname);
    FILE *f = fopen(fname, "w");

    fprintf(f, "iteration array_size total_elapsed_time\n");

    for (j=1; j<P2; j++) {
        long arrayLength = power(2,j-1)*KB;

        long lengthMod = arrayLength - 1;
        long steps =  16 * 1024 * 1024;

        for (l=0;l<N;l++)time_difs[l]=0;

        for (l=0;l<N; l++) {

            char *arr = malloc(arrayLength*sizeof(char));


            current_utc_time(&tsi);
            for (i=0; i<steps; i++) {
                long idx =(i*64) & lengthMod;       
                arr[idx]++;
            }
            current_utc_time(&tsf);


            time_difs[l] = get_time_diff(tsi, tsf);
            free(arr);
        }
        fprintf (f, "%ld %ld %lf\n", j, arrayLength/1024, avg(time_difs,N));
    }
    return 0; 

}